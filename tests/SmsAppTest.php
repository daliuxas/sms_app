<?php

use PHPUnit\Framework\TestCase;
use App\SmsApp;

class SmsAppTest extends TestCase
{
    /** @test */
    public function sms_app_sets_and_gets_params()
    {
        $smsApp = new SmsApp;
        $json   = '{"sms_list":[{"price":0.5,"income":0.41}],"required_income":11}';
        $smsApp->setParams($json);
        $this->assertEquals($json, $smsApp->getParams());
    }

    /** @test */
    public function sms_app_returns_message_when_message_limit_reached()
    {
        $smsApp = new SmsApp;
        $json   = '{"sms_list":[{"price":0.5,"income":0.41}],"required_income":11,"max_messages":2}';
        $smsApp->setParams($json);
        $this->assertEquals("Message limit reached", $smsApp->getBestPaymentOption());
    }

    /** @test */
    public function sms_app_returns_formatted_sms_payment_options_list()
    {
        $smsApp = new SmsApp;
        $json   = '{"sms_list":[{"price":0.5,"income": 0.41},{"price":1,"income":0.96},{"price":2,"income":1.91},{"price":3,"income":2.9}],"required_income":11,"max_messages":20}';
        $smsApp->setParams($json);
        $this->assertEquals([3,3,3,2,0.5], $smsApp->getBestPaymentOption());
    }

}