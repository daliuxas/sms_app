<?php

use PHPUnit\Framework\TestCase;
use App\InputValidator;

class InputValidatorTest extends TestCase
{
    /** @test */
    public function invalid_json_input_returns_false()
    {
        $validator = new InputValidator;
        $this->assertEquals(false,
            $validator->testInput('{"sms_list[{"price": 0.5,"income":0.41}],"required_income": 11}'));
    }

    /** @test */
    public function missing_data_json_input_returns_false()
    {
        $validator = new InputValidator;
        $this->assertEquals(false, $validator->testInput('{"sms_list":[{"price": 0.5,"income":0.41}]'));
        $this->assertEquals(false, $validator->testInput('{"required_income": 11}'));
    }

    /** @test */
    public function no_sms_options_returns_false()
    {
        $validator = new InputValidator;
        $this->assertEquals(false,
            $validator->testInput('{"sms_list":[],"required_income": 11}'));
    }

    /** @test */
    public function valid_json_and_good_data_returns_true()
    {
        $validator = new InputValidator;
        $this->assertEquals(true,
            $validator->testInput('{"sms_list":[{"price": 0.5,"income":0.41}],"required_income": 11}'));
    }

    /** @test */
    public function get_error_with_no_error_returns_false()
    {
        $validator = new InputValidator;
        $validator->testInput('{"sms_list":[{"price": 0.5,"income":0.41}],"required_income": 11}');
        $this->assertEquals(false, $validator->getError());
    }

    /** @test */
    public function get_error_with_error_returns_error_string()
    {
        $validator = new InputValidator;
        $validator->testInput('{"sms_list[{"price": 0.5,"income":0.41}],"required_income": 11}');
        $this->assertInternalType('string', $validator->getError());
        $validator->testInput('{"sms_list":[],"required_income": 11}');
        $this->assertInternalType('string', $validator->getError());
        $validator->testInput('{"sms_list":[{"price": 0.5,"income":0.41}]}');
        $this->assertInternalType('string', $validator->getError());
    }

}