<?php

namespace App;

class SmsApp
{
    protected $params;

    /**
     * Basic public methods for setting and getting params
     */

    public function setParams($input)
    {
        $this->params           = json_decode($input);
        $this->params->sms_list = $this->sortSmsList($this->params->sms_list);
    }

    public function getParams()
    {
        return json_encode($this->params);
    }

    /**
     * Method for sorting SMS options in params by income
     */

    protected function sortSmsList($array)
    {
        usort($array, function ($a, $b) {
            return $a->income < $b->income;
        });

        return $array;
    }

    /**
     * Method for calculating a SMS count in a list with minimum possible messages to cover then required income
     */

    protected function calculateMinSmsCount()
    {
        $minSmsCount = ceil($this->params->required_income / $this->params->sms_list[0]->income);

        return $minSmsCount;
    }

    /**
     * Methods for calculating given sms list sum price / income;
     */

    protected function calculateSmsListPriceSum($smsList)
    {
        $priceSum = 0;
        foreach ($smsList as $sms) {
            $priceSum += $sms->price;
        }

        return $priceSum;
    }

    protected function calculateSmsListIncomeSum($smsList)
    {
        $incomeSum = 0;
        foreach ($smsList as $sms) {
            $incomeSum += $sms->income;
        }

        return $incomeSum;
    }

    /**
     * Method for finding the index in params->sms_list of the smallest possible
     * sms at the end of an sms list that would still cover then required income
     */

    protected function calculateLastWholeSms($smsList)
    {
        $index = 0;
        for ($i = 0; $i < count($this->params->sms_list); $i++) {
            $tempSmsList                          = $smsList;
            $tempSmsList[count($tempSmsList) - 1] = $this->params->sms_list[$i];
            if ($this->calculateSmsListIncomeSum($tempSmsList) >= $this->params->required_income) {
                $index = $i;
            } else {
                return $index;
            }
        }

        return $index;
    }

    /**
     * Method for calculating a SMS list with minimum possible messages with default params
     */

    protected function calculateMinSmsList()
    {
        $minSmsList  = [];
        $minSmsCount = $this->calculateMinSmsCount();

        for ($x = 0; $x < $minSmsCount; $x++) {
            array_push($minSmsList, $this->params->sms_list[0]);
        }
        $minSmsList[count($minSmsList) - 1] = $this->params->sms_list[$this->calculateLastWholeSms($minSmsList)];

        return $minSmsList;
    }

    /**
     * Method that incrementally fractionates the last whole sms into smaller sms options, until there are no smaller
     * options, and returns an array of possible sms lists for payment
     */

    protected function calculateRequiredSmsListGroup()
    {
        $newSmsList   = $this->calculateMinSmsList();
        $smsListGroup = [];
        array_push($smsListGroup, $newSmsList);

        $lastWholeIndex = $this->calculateLastWholeSms($newSmsList);

        while ($lastWholeIndex < count($this->params->sms_list) - 1 && $this->checkSmsLimit(count($newSmsList) + 1)) {

            $newSmsList[count($newSmsList) - 1] = $this->params->sms_list[$lastWholeIndex + 1];
            array_push($newSmsList, $this->params->sms_list[count($this->params->sms_list) - 1]);

            for ($x = count($this->params->sms_list) - 1; $x > $lastWholeIndex; $x--) {
                $newSmsList[count($newSmsList) - 1] = $this->params->sms_list[$x];
                if ( ! ($this->calculateSmsListIncomeSum($newSmsList) < $this->params->required_income)) {
                    array_push($smsListGroup, $newSmsList);
                    $lastWholeIndex = $x;
                    break;
                }
            }
        }

        return $smsListGroup;
    }

    /**
     * Method for formatting the array of sms list objects into an array of sms prices
     */
    protected function formatSmsListGroup($smsListGroup)
    {
        $formattedSmsListGroup = [];
        foreach ($smsListGroup as $smsList) {
            $formattedSmsList = [];
            foreach ($smsList as $sms) {
                array_push($formattedSmsList, $sms->price);
            }
            array_push($formattedSmsListGroup, $formattedSmsList);
        }

        return $formattedSmsListGroup;
    }

    /**
     * Method for filtering only the cheapest lists
     */

    protected function filterPriceSmsLists($formattedSmsListGroup)
    {
        usort($formattedSmsListGroup, function ($a, $b) {
            return array_sum($a) > array_sum($b);
        });

        $filteredPriceSmsListGroup = [];

        foreach ($formattedSmsListGroup as $smsList) {
            if (array_sum($formattedSmsListGroup[0]) === array_sum($smsList)) {
                array_push($filteredPriceSmsListGroup, $smsList);
            }
        }

        return $filteredPriceSmsListGroup;
    }

    /**
     * Method for sorting the cheapest lists by sms count
     */

    protected function sortCountSmsLists($formattedSmsListGroup)
    {
        usort($formattedSmsListGroup, function ($a, $b) {
            return count($a) > count($b);
        });

        return $formattedSmsListGroup;
    }

    /**
     * Method for checking for "max_messages" param and if it wasn't violated
     */

    protected function checkSmsLimit($smsListCount)
    {
        if ( ! isset($this->params->max_messages) || isset($this->params->max_messages) && $this->params->max_messages >= $smsListCount) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Public method that finds and returns the best payment option, if it exists
     */

    public function getBestPaymentOption()
    {
        if ( ! $this->checkSmsLimit($this->calculateMinSmsCount())) {
            return "Message limit reached";
        } else {
            $smsListGroup              = $this->calculateRequiredSmsListGroup();
            $formattedSmsListGroup     = $this->formatSmsListGroup($smsListGroup);
            $filteredPriceSmsListGroup = $this->filterPriceSmsLists($formattedSmsListGroup);
            $sortedCountSmsListGroup   = $this->sortCountSmsLists($filteredPriceSmsListGroup);

            return $sortedCountSmsListGroup[0];
        }
    }
}