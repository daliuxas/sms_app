<?php

namespace App;

/**
 * class for validating data on input.json file
 */

class InputValidator
{
    protected $raw_input;
    protected $error = false;

    public function getError()
    {
        if ($this->error) {
            return $this->error;
        } else {
            return false;
        }
    }

    public function testInput($input)
    {
        if ( ! json_decode($input)) {
            $this->error = "Invalid Json";

            return false;

        } elseif ( ! property_exists(json_decode($input), 'sms_list') || ! property_exists(json_decode($input),
                'required_income')) {
            $this->error = "Missing parameters";

            return false;

        } elseif (count(json_decode($input)->sms_list) === 0) {
            $this->error = "No payment options";

            return false;

        } else {
            return true;
        }
    }

}