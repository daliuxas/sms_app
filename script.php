<?php

include_once 'app/SmsApp.php';
include_once 'app/InputValidator.php';

use App\InputValidator;
use App\SmsApp;

defined('STDIN') && $raw_input = file_get_contents($argv[1]);
$validator = new InputValidator;

if ($validator->testInput($raw_input)) {
    $smsApp = new SmsApp;
    $smsApp->setParams($raw_input);
    fwrite(STDOUT, json_encode($smsApp->getBestPaymentOption()));
} else {
    fwrite(STDOUT, $validator->getError());
}




