
**Sms app**

A small test app for calculating cheapest sms payment options from params given in an input.json file

to launch program -> php script.php input.json

to launch unit-test -> .\vendor\bin\phpunit

* don't forget to install dependencies


**Side note**

Algorhytm only works properly if the efficiency of SMS price/income increases with sms price, there are a 
number of corner cases in which thi algorhytm doesnt work, but for most cases it works properly 